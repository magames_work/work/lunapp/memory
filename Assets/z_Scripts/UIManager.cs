﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [Header("EndGamePanel Components")]
    [SerializeField]private GameObject mainPanel;
    [SerializeField]private TextMeshProUGUI winLoseTxt, bestScoreTxt, currentScoreTxt, newRecordTxt;


    [Header("Other")]
    [SerializeField]private TextMeshProUGUI healthTxt;
    


    private void Start()
    {
        if (instance == null) 
        { 
	        instance = this;
        } 
        else if(instance == this)
        { 
            Destroy(gameObject);
        }
    }

    private void Update() 
    {
        GetHealth();
        Fps();
    }

    [SerializeField]private Text fps;
    private void Fps()
    {
        fps.text = "FPS: " + (int)(1f / Time.unscaledDeltaTime);;
    }

    private void GetHealth()
    {
        healthTxt.text = "Health: " + GameManager.instance.Health.ToString();
    }

    public void Lose()
    {
        winLoseTxt.text = "You Lose!";

        if(ScoreManager.instance.Score > ScoreManager.instance.BestScore)
        {
            newRecordTxt.enabled = true;
            bestScoreTxt.text = "Previous Best Score: " + ScoreManager.instance.BestScore.ToString();
        }
        else
        {
            newRecordTxt.enabled = false;
            bestScoreTxt.text = "Best Score: " + ScoreManager.instance.BestScore.ToString();
        }
        currentScoreTxt.text = "Current Score: " + ScoreManager.instance.Score.ToString();
        

        mainPanel.SetActive(true);
    }

    public void Win()
    {
        winLoseTxt.text = "You Win!";

        if(ScoreManager.instance.Score > ScoreManager.instance.BestScore)
        {
            newRecordTxt.enabled = true;
            bestScoreTxt.text = "Previous Best Score: " + ScoreManager.instance.BestScore.ToString();
        }
        else
        {
            newRecordTxt.enabled = false;
            bestScoreTxt.text = "Best Score: " + ScoreManager.instance.BestScore.ToString();
        }

        currentScoreTxt.text = "Current score: " + ScoreManager.instance.Score.ToString();

        mainPanel.SetActive(true);
    }
}
