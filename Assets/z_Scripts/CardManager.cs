﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardManager : MonoBehaviour
{
    //singleton pattern
    public static CardManager instance;

    [Header("Config")]
    
    [SerializeField]private Material backCardMaterial;
    public Material BackCardMaterial {get{return backCardMaterial;}}

    [SerializeField]private Material[] frontCardMaterials;
    public Material[] FrontCardMaterials {get{return frontCardMaterials;}}

    [SerializeField]private int packSize, packCount;
    [SerializeField]private GameObject prefab;

    [SerializeField]private bool pickEnable = true;
    private List<Card> cards = new List<Card>();


    void Start()
    {
        if (instance == null) 
        { 
	        instance = this;
        } 
        else if(instance == this)
        { 
            Destroy(gameObject);
        }

        CreateDeck();
        RotateCards();
    }

    private void CreateDeck()
    {
    //Precalculate cards types
        int count = 0;
        int[] precalc = new int[packCount * packSize];
        for (int i = 0; i < packCount; i++)
        {
            for (int o = 0; o < packSize; o++)
            {
                precalc[count] = i;
                count++;
            }
        }
    
    //Shuffle precalculated list
        int temp;
        for (int i = 0; i < precalc.Length; i++) 
        {
             int rnd = Random.Range(0, precalc.Length);
             temp = precalc[rnd];
             precalc[rnd] = precalc[i];
             precalc[i] = temp;
        }

    //Fill grid with cards
        Grid.instance.FillGrid(prefab);

        cards.AddRange(Grid.instance.transform.GetComponentsInChildren<Card>()); 

    //Add precalculated types to cards
        int count_ = 0;
        for (int i = 0; i < packCount; i++)
        {
            for (int o = 0; o < packSize; o++)
            {
                cards[count_].ChangeType(precalc[count_]);
                count_++;
            }
        }
    }

    private void RotateCards()
    {
        StartCoroutine("IERotateCards");
    }

    private IEnumerator IERotateCards()
    {
        pickEnable = false;

        yield return new WaitForSeconds(1);

        for (int i = 0; i < cards.Count; i++)
        {
            cards[i].TurnIn();
        }

        yield return new WaitForSeconds(3);

        for (int i = 0; i < cards.Count; i++)
        {
            cards[i].TurnOut();
        }
        pickEnable = true;
    }   
    

    private Ray ray;
    private RaycastHit hit;
    private List<Card> selectedCards = new List<Card>();
    public void Pick(Camera cam)
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);

        if(pickEnable && Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(ray, out hit, 100) && hit.transform != null && hit.transform.GetComponent<Card>() && hit.transform.GetComponent<Card>().IsTurned == false)
        {
            Card card = hit.transform.GetComponent<Card>();
            card.TurnIn();
            selectedCards.Add(card);
            if(selectedCards.Count > 1 && selectedCards.Count < 4)
            {
                Match(selectedCards);
            }
        }
    }

    private void Match(List<Card> _cards)
    {
        StartCoroutine(IEMatch(_cards));
    }
    private int DstrCardCount = 0;
    private IEnumerator IEMatch(List<Card> _cards)
    {
        pickEnable = false;
        int prevCard = 0;
        bool isSuccess = false;
        for (int i = 1; i < _cards.Count; i++)
        {
            if(_cards[prevCard].cardType == _cards[i].cardType)
            {
                if(_cards.Count == 3)
                {
                    isSuccess = true;
                }
                else
                {
                    ScoreManager.instance.AddScore(1);
                }
                prevCard = i;
            }
            else
            {
                isSuccess = false;
                yield return new WaitForSeconds(1);
                for (int o = 0; o < _cards.Count; o++)
                {
                    _cards[o].TurnOut();
                }
                _cards.Clear();
                GameManager.instance.Health--;
                break;
            }
        }
        if(isSuccess)
        {
            ScoreManager.instance.AddScore(3);

            yield return new WaitForSeconds(1f);

            for (int i = 0; i < _cards.Count; i++)
            {
                _cards[i].gameObject.SetActive(false);
                DstrCardCount++;
            }

            if(DstrCardCount >= packCount * packSize)
                GameManager.instance.EndGame();
            else
            {
                _cards.Clear();
            }
        }
        pickEnable = true;
    }
    
    
}
