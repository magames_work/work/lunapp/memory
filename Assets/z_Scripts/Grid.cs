﻿using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    //singleton pattern
    public static Grid instance = null;

    [Header("Config")]
    [SerializeField]private Vector2 startPos;
    [SerializeField]private Vector2 sellCount;
    [SerializeField]private Vector2 sellGap;
    [SerializeField]private Vector3 itemSize;

    private List<Vector3> gridPositions = new List<Vector3>();
    
    private void Start()
    {
        if (instance == null) 
        { 
	        instance = this;
        } 
        else if(instance == this)
        { 
            Destroy(gameObject);
        }

        CreateGrid();
    }

    private void CreateGrid()
    {
        //TODO: Сделать, чтобы сетка соответствовала экрану
        Vector3 position;
        for (int i = 0; i < sellCount.x * sellCount.y; i++)
        {
            position = new Vector3(startPos.x + (sellGap.x * (i % sellCount.x)), startPos.y + (-sellGap.y * (i % sellCount.y)));
            gridPositions.Add(position);
        }
    }

    public void FillGrid(GameObject prefab)
    {
        prefab.transform.localScale = itemSize;
        for (int i = 0; i < gridPositions.Count; i++)
        {
            Instantiate(prefab, gridPositions[i], Quaternion.identity, transform);
        }
    }
    public void FillGrid(GameObject[] prefabs)
    {
        for (int i = 0; i < gridPositions.Count; i++)
        {
            prefabs[i].transform.localScale = itemSize;
            Instantiate(prefabs[i], gridPositions[i], Quaternion.identity, transform);
        }
    }
}
