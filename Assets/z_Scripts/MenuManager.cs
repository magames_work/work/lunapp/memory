﻿using UnityEngine;
using TMPro;

public class MenuManager : MonoBehaviour
{
    [SerializeField]private TextMeshProUGUI menuBestScore;

    void Start()
    {
        if(menuBestScore)
            menuBestScore.text = "Best Score: " + PlayerPrefs.GetInt("BestScore");
    }
}
