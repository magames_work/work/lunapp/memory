﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class _SceneManager : MonoBehaviour
{
    public void SwitchScene(int id)
    {
        SceneManager.LoadScene(id);
    }
}
