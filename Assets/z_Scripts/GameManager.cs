﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    private int health;
    public int Health {
        get{
            return health;
        }
        set{
            health = value;
            if(health < 1)
            {
                EndGame();
                health = 0;
            }
        }
    }
    
    [SerializeField] private int startHealth;
    private Camera cam;

    private void Start()
    {
        if (instance == null) 
        { 
	        instance = this;
        } 
        else if(instance == this)
        { 
            Destroy(gameObject);
        }

        health = startHealth;
        cam = Camera.main;

        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        CardManager.instance.Pick(cam);
    }

    public void EndGame()
    {
        if(Health <= 0)
        {
            print("you lose");
            UIManager.instance.Lose();
        }
        else
        {
            print("you win");
            UIManager.instance.Win();
        }

        ScoreManager.instance.SaveScore();
    }
}
