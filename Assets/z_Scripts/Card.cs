﻿using UnityEngine;

public class Card : MonoBehaviour
{
    private MeshRenderer frontCard, backCard;
    public int cardType;

    private bool isTurned;
    public bool IsTurned {get {return isTurned;}} 

    private Animator animator;

    private void Start() 
    {
        animator = GetComponent<Animator>();
        frontCard = transform.GetChild(0).GetComponent<MeshRenderer>();
        backCard = transform.GetChild(1).GetComponent<MeshRenderer>();

        backCard.material = CardManager.instance.BackCardMaterial;
    }

    public void ChangeType(int type)
    {
        frontCard = transform.GetChild(0).GetComponent<MeshRenderer>();
        this.cardType = type;
        frontCard.material = CardManager.instance.FrontCardMaterials[type];
    }

    public void TurnIn()
    {
        if(!isTurned)
        {
            //this.transform.eulerAngles = Vector3.up * 180;
            animator.SetTrigger("TurnIn");
            isTurned = true;
        }
    }
    public void TurnOut()
    {
        if(isTurned)
        {
            //this.transform.eulerAngles = Vector3.up * 0;
            animator.SetTrigger("TurnOut");
            isTurned = false;
        }
    }
}
