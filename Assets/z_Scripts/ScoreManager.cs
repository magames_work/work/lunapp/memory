﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    private int score;
    public int Score {get{return score;}}

    private int bestScore;
    public int BestScore {get{return bestScore;}}
    void Start()
    {
        score = 0;
        if (instance == null) 
        { 
	        instance = this;
        } 
        else if(instance == this)
        { 
            Destroy(gameObject);
        }

        if(!PlayerPrefs.HasKey("BestScore"))
            bestScore = 0;
        else
            bestScore = PlayerPrefs.GetInt("BestScore");
    }

    public void AddScore(int multiply)
    {
        score += multiply * GameManager.instance.Health;
    }

    public void SaveScore()
    {
        if(PlayerPrefs.GetInt("BestScore") < score)
        {
            bestScore = score;
            PlayerPrefs.SetInt("BestScore", bestScore);
        }
    }
}
